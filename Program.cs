﻿using System;

namespace variable_examples_3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Type in any numeric number.");

            var usernum = int.Parse(Console.ReadLine());

            Console.WriteLine($"The number you typed when multiplied by 3 is {usernum * 3}");
        }
    }
}
